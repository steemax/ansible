#!/bin/bash
RED='\033[0;31m'
GREEN='\033[0;32m'
YELLOW='\033[0;33m'
WHITE='\033[0;37m'
SMH="2.0.8-0"
KUB47="v1.20"
KUB48="v1.21"
KUB49="v1.22"
while getopts k:c:s:h: flag
do
	case "${flag}" in
		k) KBCFG=${OPTARG};;
		c) CLUSTERC=${OPTARG};;
		s) SILENT=${OPTARG};;
		h) ;;
	esac
done
if [[ -n $1 ]] && [[ "${1#*.}" == "-h" ]]; then
	echo "check-cluster-2line.sh use:"
	echo "-k /path/to/kubeconfig"
	echo "-c cluster name (e.g. psi-gen.ca.sbrf.ru)"
	echo "-s true - enable silent mode for zabbix, without - interactive mode"
	echo "-h - this help"
    exit 0;
fi
if [[ $SILENT == "true" ]]; then
	if [[ $KBCFG == "" ]]; then
		echo "please add -k path_to_kubeconfig in start arguments"
		exit 0;
	else
		if [[ $CLUSTERC == "" ]]; then
			echo "please add -c clustername (e.g. psi-gen.ca.sbrf.ru) in start arguments"
			exit 0;
		else
			if [[ `grep $CLUSTERC $KBCFG` != "" ]]; then
				echo "Silent mode enabled"
				export KUBECONFIG=$KBCFG
				alias oc="`which oc |grep -v alias |awk '{print $NF}'` --kubeconfig=$KBCFG --insecure-skip-tls-verify"
				rm -f /tmp/scoring_cluster
				cp $KBCFG /tmp/kbcfg
				CLUSTER=`echo $CLUSTERC`
				DEVPROM=`echo $CLUSTER |grep ^prom`
				IPBAST=`dig bastion.$CLUSTER |grep 'ANSWER SECTION' -A2 |grep -vE 'ANSWER SECTION|^$' |awk '{print $NF}'`
				IPAPI=`dig api.$CLUSTER |grep 'ANSWER SECTION' -A2 |grep -vE 'ANSWER SECTION|^$' |awk '{print $NF}'`
				IPAPIINT=`dig api-int.$CLUSTER |grep 'ANSWER SECTION' -A2 |grep -vE 'ANSWER SECTION|^$' |awk '{print $NF}'`
				IPAPPS=`dig 123.apps.$CLUSTER |grep 'ANSWER SECTION' -A2 |grep -vE 'ANSWER SECTION|^$' |awk '{print $NF}'`
				ISSUERALPHA="SberCA Int"
				ISSUERSIGMA="SberCA Ext"
				if [[ `echo $CLUSTER |grep ca.sbrf` == "" ]]; then 
					NETSEG="sigma"; 
				else 
					NETSEG="alpha";
				fi
				PRENAME=`oc --insecure-skip-tls-verify get po -n openshift-ingress -l ingresscontroller.operator.openshift.io/deployment-ingresscontroller=default -o wide |awk '{print $7}' |grep -v NODE`
				ROUTERNAME=`for i in $PRENAME; do oc --insecure-skip-tls-verify get no $i -o wide |grep CoreOS |awk '{print $6}'; done`
				PRENAMEF=`oc --insecure-skip-tls-verify get po -n openshift-ingress -o wide |awk '{print $7}' |grep -v NODE`
				ROUTERFULL=`for i in $PRENAMEF; do oc --insecure-skip-tls-verify get no $i -o wide |grep CoreOS |awk '{print $6}'; done`
				MASTERNAME=`oc --insecure-skip-tls-verify get no |grep master |awk '{print $1}'`
				CLUSTVERS=`oc --insecure-skip-tls-verify describe clusterversion |grep -v Update |grep Desired -A3 |grep Version |awk '{print $2}' |awk -F . '{print $1"."$2}'`
			else
				echo "Your cluster name not match with cluster name in kubeconfig provided, abort"
				exit 0;
			fi
		fi
	fi
	
else
	echo "Interactive mode enabled"
	echo "Enter cluster name (e.g. psi-gen.ca.sbrf.ru):"
    read CLUSTER
    DEVPROM=`echo $CLUSTER |grep ^prom`
    IPBAST=`dig bastion.$CLUSTER |grep 'ANSWER SECTION' -A2 |grep -vE 'ANSWER SECTION|^$' |awk '{print $NF}'`
    IPAPI=`dig api.$CLUSTER |grep 'ANSWER SECTION' -A2 |grep -vE 'ANSWER SECTION|^$' |awk '{print $NF}'`
    IPAPIINT=`dig api-int.$CLUSTER |grep 'ANSWER SECTION' -A2 |grep -vE 'ANSWER SECTION|^$' |awk '{print $NF}'`
    IPAPPS=`dig 123.apps.$CLUSTER |grep 'ANSWER SECTION' -A2 |grep -vE 'ANSWER SECTION|^$' |awk '{print $NF}'`
    ISSUERALPHA="SberCA Int"
    ISSUERSIGMA="SberCA Ext"
    if [[ `echo $CLUSTER |grep ca.sbrf` == "" ]]; then 
		NETSEG="sigma"; 
	else 
	    NETSEG="alpha";
	fi
    echo "Enter OpenShift Cluster user name:"
    read UN
    oc --insecure-skip-tls-verify login --server https://api.$CLUSTER:6443 --kubeconfig=/tmp/kbcfg -u $UN
    if [[ $? = 1 ]]; then 
    	echo "Login failed (401 Unauthorized), please retry..."; 
    	exit 0;
    fi
    PRENAME=`oc --insecure-skip-tls-verify get po -n openshift-ingress -l ingresscontroller.operator.openshift.io/deployment-ingresscontroller=default -o wide --kubeconfig=/tmp/kbcfg |awk '{print $7}' |grep -v NODE`
    ROUTERNAME=`for i in $PRENAME; do oc --insecure-skip-tls-verify get no $i -o wide --kubeconfig=/tmp/kbcfg |grep CoreOS |awk '{print $6}'; done`
    PRENAMEF=`oc --insecure-skip-tls-verify get po -n openshift-ingress -o wide --kubeconfig=/tmp/kbcfg |awk '{print $7}' |grep -v NODE`
    ROUTERFULL=`for i in $PRENAMEF; do oc --insecure-skip-tls-verify get no $i -o wide --kubeconfig=/tmp/kbcfg |grep CoreOS |awk '{print $6}'; done`
    MASTERNAME=`oc --insecure-skip-tls-verify get no --kubeconfig=/tmp/kbcfg |grep master |awk '{print $1}'`
    CLUSTVERS=`oc --insecure-skip-tls-verify describe clusterversion --kubeconfig=/tmp/kbcfg |grep -v Update |grep Desired -A3 |grep Version |awk '{print $2}' |awk -F . '{print $1"."$2}'`
fi

function CheckIPDns {
if [[ $SILENT == "true" ]]; then
	if [[ $IPBAST == $IPAPI ]]; then
		echo "APIDNS:1" >> /tmp/scoring_cluster
	else
		echo "APIDNS:0" >> /tmp/scoring_cluster
	fi
else
	if [[ $IPBAST == $IPAPI ]]; then
		echo -e "${WHITE}Check API address does not belong bastion:\t\t\t ${RED}false"
	else
		echo -e "${WHITE}Check API address does not belong bastion:\t\t\t ${GREEN}pass"
	fi
fi
if [[ $SILENT == "true" ]]; then
	if [[ $IPBAST == $IPAPIINT ]]; then
		echo "INTDNS:1" >> /tmp/scoring_cluster
	else
		echo "INTDNS:0" >> /tmp/scoring_cluster
	fi
else
	if [[ $IPBAST == $IPAPIINT ]]; then
		echo -e "${WHITE}Check API-INT address does not belong bastion:\t\t\t ${RED}false"
	else
		echo -e "${WHITE}Check API-INT address does not belong bastion:\t\t\t ${GREEN}pass"
	fi
fi
if [[ $SILENT == "true" ]]; then
	if [[ $IPBAST == $IPAPPS ]]; then
		echo "APPDNS:1" >> /tmp/scoring_cluster
	else
		echo "APPDNS:0" >> /tmp/scoring_cluster
	fi
else
	if [[ $IPBAST == $IPAPPS ]]; then
		echo -e "${WHITE}Check *APPS address does not belong bastion:\t\t\t ${RED}false"
	else
		echo -e "${WHITE}Check *APPS address does not belong bastion:\t\t\t ${GREEN}pass"
	fi
fi
}


function CheckOmegaAuth {
if [[ $SILENT == "true" ]]; then
	AUTHPROV=`oc --insecure-skip-tls-verify get oauths.config.openshift.io cluster -o yaml |grep 'name: omega' |sed 's/ //g'`
	if [[ $NETSEG != "sigma" ]]; then
		if [[ $AUTHPROV != "name:omega" ]]; then
			echo "OMAUTH:1" >> /tmp/scoring_cluster
		else
			echo "OMAUTH:0" >> /tmp/scoring_cluster
		fi
	else
		echo "OMAUTH:0" >> /tmp/scoring_cluster
	fi
else
	AUTHPROV=`oc --insecure-skip-tls-verify get oauths.config.openshift.io cluster -o yaml --kubeconfig=/tmp/kbcfg |grep 'name: omega' |sed 's/ //g'`
	if [[ $NETSEG == "sigma" ]]; then
		echo -e "${WHITE}Check OMEGA auth proider configured:\t\t\t\t ${GREEN}pass"
	else
		if [[ $AUTHPROV == "name:omega" ]]; then
			echo -e "${WHITE}Check OMEGA auth proider configured:\t\t\t\t ${GREEN}pass"
		else
			echo -e "${WHITE}Check OMEGA auth proider configured:\t\t\t\t ${RED}false"
		fi
	fi
fi
}

function CheckFALSENode {
if [[ $SILENT == "true" ]]; then
	STNODE=`oc --insecure-skip-tls-verify get no |grep -E 'Disa|NotRe'`
	if [[ $STNODE == "" ]]; then
		echo "DISBNO:0" >> /tmp/scoring_cluster
	else
		echo "DISBNO:1" >> /tmp/scoring_cluster
	fi
else
	STNODE=`oc --insecure-skip-tls-verify get no --kubeconfig=/tmp/kbcfg |grep -E 'Disa|NotRe'`
	if [[ $STNODE == "" ]]; then
		echo -e "${WHITE}Check SchedulingDisabled or NotReady nodes:\t\t\t ${GREEN}pass"
	else
		echo -e "${WHITE}Check SchedulingDisabled or NotReady nodes:\t\t\t ${RED}false"
	fi
fi
}

function CheckMasterHDD {
MASTER=`oc --insecure-skip-tls-verify get no --kubeconfig=/tmp/kbcfg |grep master |awk '{print $1}'`
for i in $MASTER; do ssh -o "StrictHostKeyChecking no" core@$i lsblk |grep ^sda |awk {'print $4'} |awk -F G {'print $1'} >> /tmp/masterdisk; done
MSIZE=`cat /tmp/masterdisk |sort -u`
if [[ $MSIZE -ge 500 ]]; then
	echo -e "${WHITE}Check disk size on masters 500G or gt:\t\t\t\t ${GREEN} pass"
else
	echo -e "${WHITE}Check disk size on masters 500G or gt:\t\t\t\t ${RED} false"
fi
}

function CheckCertsAPI {
ISSUER=`echo |openssl s_client -servername api.$CLUSTER -connect api.$CLUSTER:6443 2>/dev/null |grep issuer |awk -F = {'print $NF'}`
if [[ $NETSEG == "sigma" ]]; then
    if [[ $ISSUER == $ISSUERSIGMA ]]; then
		if [[ $SILENT == "true" ]]; then
			echo "APICRT:0" >> /tmp/scoring_cluster
		else
			echo -e "${WHITE}Check API Certificate issuer:\t\t\t\t\t ${GREEN}pass"
		fi
	else
		if [[ $SILENT == "true" ]]; then
			echo "APICRT:1" >> /tmp/scoring_cluster
		else
			echo -e "${WHITE}Check API Certificate issuer:\t\t\t\t\t ${RED}false"
			echo -e "${YELLOW}\t your cert signed by $ISSUER...correct is $ISSUERSIGMA"
		fi
	fi
else
	if [[ $ISSUER == $ISSUERALPHA ]]; then
		if [[ $SILENT == "true" ]]; then
			echo "APICRT:0" >> /tmp/scoring_cluster
		else
			echo -e "${WHITE}Check API Certificate issuer:\t\t\t\t\t ${GREEN}pass"
		fi
	else
		if [[ $SILENT == "true" ]]; then
			echo "APICRT:1" >> /tmp/scoring_cluster
		else
			echo -e "${WHITE}Check API Certificate issuer:\t\t\t\t\t ${RED}false"
			echo -e "${YELLOW}\t your cert signed by $ISSUER...correct is $ISSUERALPHA"
		fi
	fi
fi
}

function CheckCertsAPPS {
ISSUER=`echo |openssl s_client -servername 123.apps.$CLUSTER -connect 123.apps.$CLUSTER:443 2>/dev/null |grep issuer |awk -F = {'print $NF'}`
if [[ $NETSEG == "sigma" ]]; then
    if [[ $ISSUER == $ISSUERSIGMA ]]; then
		if [[ $SILENT == "true" ]]; then
			echo "APPCRT:0" >> /tmp/scoring_cluster
		else
			echo -e "${WHITE}Check APPS Certificate issuer:\t\t\t\t\t ${GREEN}pass"
		fi
	else
		if [[ $SILENT == "true" ]]; then
			echo "APPCRT:1" >> /tmp/scoring_cluster
		else
			echo -e "${WHITE}Check APPS Certificate issuer:\t\t\t\t\t ${RED}false"
			echo -e "${YELLOW}\t your cert signed by $ISSUER...correct is $ISSUERSIGMA"
		fi
	fi
else
	if [[ $ISSUER == $ISSUERALPHA ]]; then
		if [[ $SILENT == "true" ]]; then
			echo "APPCRT:0" >> /tmp/scoring_cluster
		else
			echo -e "${WHITE}Check APPS Certificate issuer:\t\t\t\t\t ${GREEN}pass"
		fi
	else
		if [[ $SILENT == "true" ]]; then
			echo "APPCRT:1" >> /tmp/scoring_cluster
		else
			echo -e "${WHITE}Check APPS Certificate issuer:\t\t\t\t\t ${RED}false"
			echo -e "${YELLOW}\t your cert signed by $ISSUER...correct is $ISSUERALPHA"
		fi
	fi
fi
}

function CheckCompleteBackup {
if [[ $SILENT == "true" ]]; then
	BACKTRIG=`oc --insecure-skip-tls-verify get po -n firepaws-etcd-backup |grep Complete`
	if [[ $BACKTRIG == "" ]]; then
		echo "BKPCMP:1" >> /tmp/scoring_cluster
	else
		echo "BKPCMP:0" >> /tmp/scoring_cluster
	fi
else
	BACKTRIG=`oc --insecure-skip-tls-verify get po -n firepaws-etcd-backup --kubeconfig=/tmp/kbcfg |grep Complete`
	if [[ $BACKTRIG == "" ]]; then
		echo -e "${WHITE}Check Complete etcd backup pod:\t\t\t\t\t ${RED}false"
	else
		echo -e "${WHITE}Check Complete etcd backup pod:\t\t\t\t\t ${GREEN}pass"
	fi
fi
}

function CheckMCPShedState {

if [[ $DEVPROM == "" ]]; then
	if [[ $SILENT == "true" ]]; then
		for i in `oc --insecure-skip-tls-verify get mcp --kubeconfig=/tmp/kbcfg |awk '{print $1}' |grep -v NAME`; do if [[ `oc --insecure-skip-tls-verify describe mcp $i --kubeconfig=/tmp/kbcfg |grep Paused |sed 's/ //g' |grep Paused:true` != "" ]]; then echo 1 >> /tmp/mcpsilent; else echo 0 >> /tmp/mcpsilent; fi; done
		if [[ `grep 1 /tmp/mcpsilent` != "" ]]; then
			echo "MCPPAU:1" >> /tmp/scoring_cluster
		else
			echo "MCPPAU:0" >> /tmp/scoring_cluster
		fi
	else
		echo -e "${WHITE}Check that MCP pause state:"
		for i in `oc --insecure-skip-tls-verify get mcp --kubeconfig=/tmp/kbcfg |awk '{print $1}' |grep -v NAME`; do if [[ `oc --insecure-skip-tls-verify describe mcp $i --kubeconfig=/tmp/kbcfg |grep Paused |sed 's/ //g' |grep Paused:true` != "" ]]; then echo -e "${YELLOW}\tMCP $i in pause state now, please repair it."; else echo -e "${GREEN}\tMCP $i in not pause state now, it's ok."; fi; done
	fi
else
	PODREPL=`oc --insecure-skip-tls-verify get deployments firepaws-mcp-operator-controller-manager -n firepaws-mcp-operator --template='{{ .spec.replicas }}' --kubeconfig=/tmp/kbcfg`
	if [[ $PODREPL == "0" ]]; then
		if [[ $SILENT == "true" ]]; then
			echo "MCPPOD:1" >> /tmp/scoring_cluster
		else
			echo -e "${WHITE}\tplease ${RED}ENABLE ${WHITE}firepaws-mcp-operator-controller-manager, now replicas = $PODREPL"
		fi
	fi
	if [[ $SILENT == "true" ]]; then
		for i in `oc --insecure-skip-tls-verify get mcp --kubeconfig=/tmp/kbcfg |awk '{print $1}' |grep -v NAME`; do if [[ `oc --insecure-skip-tls-verify describe mcp $i --kubeconfig=/tmp/kbcfg |grep Paused |sed 's/ //g' |grep Paused:false` != "" ]]; then echo 1 >> /tmp/mcpsilent; else echo 0 >> /tmp/mcpsilent; fi; done
		if [[ `grep 1 /tmp/mcpsilent` != "" ]]; then
			echo "MCPPAU:1" >> /tmp/scoring_cluster
		else
			echo "MCPPAU:0" >> /tmp/scoring_cluster
		fi
	else
		for i in `oc --insecure-skip-tls-verify get mcp --kubeconfig=/tmp/kbcfg |awk '{print $1}' |grep -v NAME`; do if [[ `oc --insecure-skip-tls-verify describe mcp $i --kubeconfig=/tmp/kbcfg |grep Paused |sed 's/ //g' |grep Paused:false` != "" ]]; then echo -e "${YELLOW}\tMCP $i in NOT pause state now, please repair it."; fi; done
	fi
fi
}

function CheckCompleteGSync {
GSYNC=`oc --insecure-skip-tls-verify get po -n firepaws-groups-synchronizer --kubeconfig=/tmp/kbcfg |grep Complete`
if [[ $GSYNC == "" ]]; then
	if [[ $SILENT == "true" ]]; then
		echo "GPSPOD:1" >> /tmp/scoring_cluster
	else
		echo -e "${WHITE}Check Complete groups-synchronizer pod:\t\t\t\t ${RED}false"
	fi
else
	if [[ $SILENT == "true" ]]; then
		echo "GPSPOD:0" >> /tmp/scoring_cluster
	else
		echo -e "${WHITE}Check Complete groups-synchronizer pod:\t\t\t\t ${GREEN}pass"
	fi
fi
}

function CheckRFSRouter {
if [[ $ROUTERNAME != "" ]]; then
	for i in $ROUTERNAME; do ssh -o "StrictHostKeyChecking no" core@$i cat /sys/class/net/ens192/queues/rx-*/rps_flow_cnt |sort -u >> /tmp/checkrfs; done
	RFSSTATE=`cat /tmp/checkrfs |sort -u`
	if [[ $RFSSTATE == "0" ]]; then
		if [[ $SILENT == "true" ]]; then
			echo "TCPRFS:1" >> /tmp/scoring_cluster
		else
			echo -e "${WHITE}Check Receive Flow Streeng (RFS) on router RHCOS:\t\t ${RED}false"
		fi
	else
		if [[ $SILENT == "true" ]]; then
			if [[ `cat /tmp/checkrfs |sort -u |wc -l` == "1" ]]; then
				echo "TCPRFS:0" >> /tmp/scoring_cluster
			else
				echo "TCPRFS:1" >> /tmp/scoring_cluster
			fi
		else
			if [[ `cat /tmp/checkrfs |sort -u |wc -l` == "1" ]]; then
				echo -e "${WHITE}Check Receive Flow Streeng (RFS) on router RHCOS:\t\t ${GREEN}pass"
			else
				echo -e "${WHITE}Check Receive Flow Streeng (RFS) on router RHCOS:\t\t ${RED}false"
			fi
		fi
	fi
else
	if [[ $SILENT != "true" ]]; then
		echo -e "${WHITE}Check Receive Flow Streeng (RFS) on router RHCOS:"
		echo -e "${YELLOW}\tnot found router RHCOS skipping check...${WHITE}"
	fi
fi
}

function CheckSMHVers {
SNAME=`oc --insecure-skip-tls-verify get olm --kubeconfig=/tmp/kbcfg |grep 'Red Hat OpenShift Service Mesh' |awk '{print $1}'`
if [[ $SNAME != "" ]]; then
	SVERS=`oc --insecure-skip-tls-verify get $SNAME --template='{{ .spec.version }}' --kubeconfig=/tmp/kbcfg`
fi
if [[ $SMH == $SVERS ]]; then
	if [[ $SILENT == "true" ]]; then 
		echo "RHSMHV:0" >> /tmp/scoring_cluster
	else
		echo -e "${WHITE}Check RHSMH installed version = $SMH:\t\t\t ${GREEN}pass"
	fi
else
	if [[ $SILENT == "true" ]]; then
		echo "RHSMHV:1" >> /tmp/scoring_cluster
	else
		echo -e "${WHITE}Check RHSMH installed version = $SMH:\t\t\t ${RED}false"
		echo -e "${YELLOW}\t installed version in $SVERS"
	fi
fi
}

function CheckJaegVers {
JANAME=`oc --insecure-skip-tls-verify get olm --kubeconfig=/tmp/kbcfg |grep 'Red Hat OpenShift Jaeger' |awk '{print $1}'`
if [[ $JANAME == "" ]]; then
	echo -e "${WHITE}Check OpenShift Jaeger Operator installed:\t\t\t ${RED}false"
else
	echo -e "${WHITE}Check OpenShift Jaeger Operator installed:\t\t\t ${GREEN}pass"
fi
}

function CheckKialiVers {
KINAME=`oc --insecure-skip-tls-verify get olm --kubeconfig=/tmp/kbcfg |grep -E 'Kiali|Red Hat OpenShift distributed tracing platform' |awk '{print $1}'`
if [[ $KINAME == "" ]]; then
	echo -e "${WHITE}Check OpenShift distributed tracing platform installed:\t\t ${RED}false"
else
	echo -e "${WHITE}Check OpenShift distributed tracing platform installed:\t\t ${GREEN}pass"
fi
}

function CheckRouterHW {
if [[ $ROUTERFULL != "" ]]; then
	for i in $ROUTERFULL; do MEM=`ssh -o "StrictHostKeyChecking no" core@$i cat /proc/meminfo |grep MemTotal |awk '{print $2}'`; let MEM1="$MEM/1024/1024"; echo $MEM1 > /tmp/$i-mem; done
	for i in $ROUTERFULL; do ssh -o "StrictHostKeyChecking no" core@$i cat /proc/cpuinfo |grep 'physical id' |wc -l > /tmp/$i-cpu; done
	for i in $ROUTERFULL; do ssh -o "StrictHostKeyChecking no" core@$i lsblk |grep ^sda |awk '{print $4}' |awk -FG '{print $1}' > /tmp/$i-disk; done
	if [[ $SILENT == "true" ]]; then
		for i in $ROUTERFULL; do 
		if [[ `cat /tmp/$i-mem` -ge "60" ]]; then
			if [[ `cat /tmp/$i-cpu` -ge "16" ]]; then
				if [[ `cat /tmp/$i-disk` -ge "200" ]]; then
					echo 0 >> /tmp/routerhealthsilent;
				else
					echo 1 >> /tmp/routerhealthsilent;
				fi
			else
				echo 1 >> /tmp/routerhealthsilent;
			fi
		else
			echo 1 >> /tmp/routerhealthsilent;
		fi;
		done
		if [[ `grep 1 /tmp/routerhealthsilent` == "" ]]; then
			echo "ROUTHW:0" >> /tmp/scoring_cluster
		else
			echo "ROUTHW:5" >> /tmp/scoring_cluster
		fi
	else
		echo -e "${WHITE}Check router hardware 16/64/200 (only for RHCOS):"
		for i in $ROUTERFULL; do 
			if [[ `cat /tmp/$i-mem` -ge "60" ]]; then
				if [[ `cat /tmp/$i-cpu` -ge "16" ]]; then
					if [[ `cat /tmp/$i-disk` -ge "200" ]]; then
						echo -e "${WHITE}\t$i hardware requirement is ${GREEN}\t\t\t pass";
					else
						echo -e "${WHITE}\t$i hardware requirement is ${RED}\t\t\t fail";
					fi
				else
					echo -e "${WHITE}\t$i hardware requirement is ${RED}\t\t\t fail";
				fi
			else
				echo -e "${WHITE}\t$i hardware requirement is ${RED}\t\t\t fail";
			fi;
		done
	fi
else
	if [[ $SILENT != "true" ]]; then
		echo -e "${WHITE}Check Receive Flow Streeng (RFS) on router RHCOS:"
		echo -e "${YELLOW}\tnot found router RHCOS skipping check...${WHITE}"
	fi
fi	
}

function CheckMasterHW {
for i in $MASTERNAME; do MEM=`ssh -o "StrictHostKeyChecking no" core@$i cat /proc/meminfo |grep MemTotal |awk '{print $2}'`; let MEM1="$MEM/1024/1024"; echo $MEM1 > /tmp/$i-mem; done
for i in $MASTERNAME; do ssh -o "StrictHostKeyChecking no" core@$i cat /proc/cpuinfo |grep 'physical id' |wc -l > /tmp/$i-cpu; done
for i in $MASTERNAME; do ssh -o "StrictHostKeyChecking no" core@$i lsblk |grep ^sda |awk '{print $4}' |awk -FG '{print $1}' > /tmp/$i-disk; done
if [[ $SILENT == "true" ]]; then
	for i in $MASTERNAME; do 
		if [[ `cat /tmp/$i-mem` -ge "60" ]]; then
			if [[ `cat /tmp/$i-cpu` -ge "16" ]]; then
				if [[ `cat /tmp/$i-disk` -ge "500" ]]; then
					echo 0 >> /tmp/masterhealthsilent;
				else
					echo 1 >> /tmp/masterhealthsilent;
				fi
			else
				echo 1 >> /tmp/masterhealthsilent;
			fi
		else
			echo 1 >> /tmp/masterhealthsilent;
		fi;
	done
	if [[ `grep 1 /tmp/masterhealthsilent` == "" ]]; then
			echo "MASTHW:0" >> /tmp/scoring_cluster
		else
			echo "MASTHW:5" >> /tmp/scoring_cluster
	fi
else
	echo -e "${WHITE}Check master hardware 16/64/500:"
	for i in $MASTERNAME; do 
		if [[ `cat /tmp/$i-mem` -ge "60" ]]; then
			if [[ `cat /tmp/$i-cpu` -ge "16" ]]; then
				if [[ `cat /tmp/$i-disk` -ge "500" ]]; then
					echo -e "${WHITE}\t$i hardware requirement is ${GREEN}\t pass";
				else
					echo -e "${WHITE}\t$i hardware requirement is ${RED}\t fail";
				fi
			else
				echo -e "${WHITE}\t$i hardware requirement is ${RED}\t fail";
			fi
		else
			echo -e "${WHITE}\t$i hardware requirement is ${RED}\t fail";
		fi;
	done
fi
}

function CheckNotLabeledNodes {
COUNTS=`oc --insecure-skip-tls-verify get mcp worker --template='{{ .status.machineCount }}' --kubeconfig=/tmp/kbcfg`
if [[ $SILENT == "true" ]]; then
	if [[ $COUNTS == "0" ]]; then
		echo "NODENL:0" >> /tmp/scoring_cluster
	else
		echo "NODENL:1" >> /tmp/scoring_cluster
	fi
else
	if [[ $COUNTS == "0" ]]; then
		echo -e "${WHITE}Check not labeled nodes (worker mcp):\t\t\t\t ${GREEN}pass"
	else
		echo -e "${WHITE}Check not labeled nodes (worker mcp):\t\t\t\t ${RED}false"
		echo -e "${YELLOW}\t you have $COUNTS not labeled servers, please check it in worker pool"
	fi
fi
}

function CheckMasterSchedulable {
MSCHED=`oc --insecure-skip-tls-verify get schedulers.config.openshift.io cluster --template='{{ .spec.mastersSchedulable }}' --kubeconfig=/tmp/kbcfg`
if [[ $MSCHED == "false" ]]; then
	if [[ $SILENT == "true" ]]; then
		echo "MASTSH:0" >> /tmp/scoring_cluster
	else
		echo -e "${WHITE}Check master Schedulable is false:\t\t\t\t ${GREEN}pass"
	fi
else
	if [[ $SILENT == "true" ]]; then
		echo "MASTSH:1" >> /tmp/scoring_cluster
	else
		echo -e "${WHITE}Check master Schedulable is false:\t\t\t\t ${RED}false"
		echo -e "${YELLOW}\t please repair it in schedulers.config.openshift.io cluster"
	fi
fi
}

function CheckRHELUpgrade {
RHELVERSKUB=`oc --insecure-skip-tls-verify get no -o wide --kubeconfig=/tmp/kbcfg |grep 'Red Hat Enterprise Linux Server' |awk '{print $5}' |awk -F . '{print $1"."$2}' |sort -u`
if [[ $RHELVERSKUB == "" ]]; then
	if [[ $SILENT == "true" ]]; then
		echo "RHUPGS:0" >> /tmp/scoring_cluster
	else
		echo -e "${WHITE}Check update status RHEL nodes (upgrade step complete):\t\t ${GREEN}pass"
	fi
else
	if [[ $CLUSTVERS == "4.9" ]]; then
		if [[ $KUB49 == $RHELVERSKUB ]]; then
			if [[ $SILENT == "true" ]]; then
				echo "RHUPGS:0" >> /tmp/scoring_cluster
			else
				echo -e "${WHITE}Check update status RHEL nodes (upgrade step complete):\t\t ${GREEN}pass"
			fi
		else
			if [[ $SILENT == "true" ]]; then
				echo "RHUPGS:1" >> /tmp/scoring_cluster
			else
				echo -e "${WHITE}Check update status RHEL nodes (upgrade step complete):\t\t ${RED}false"
				echo -e "${YELLOW}\tplease check not upgraded nodes:"
				FAILN=`oc --insecure-skip-tls-verify get no -o wide --kubeconfig=/tmp/kbcfg |grep -v $KUB49 |grep -v NAME |awk '{print $1}'`
				for i in $FAILN; do echo -e "${WHITE}\t\t$i"; done
			fi
		fi
	else
		if [[ $CLUSTVERS == "4.8" ]]; then
			if [[ $KUB48 == $RHELVERSKUB ]]; then
				if [[ $SILENT == "true" ]]; then
					echo "RHUPGS:0" >> /tmp/scoring_cluster
				else
					echo -e "${WHITE}Check update status RHEL nodes (upgrade step complete):\t\t ${GREEN}pass"
				fi
			else
				if [[ $SILENT == "true" ]]; then
					echo "RHUPGS:1" >> /tmp/scoring_cluster
				else
					echo -e "${WHITE}Check update status RHEL nodes (upgrade step complete):\t\t ${RED}false"
					echo -e "${YELLOW}\tplease check not upgraded nodes:"
					FAILN=`oc --insecure-skip-tls-verify get no -o wide --kubeconfig=/tmp/kbcfg |grep -v $KUB48 |grep -v NAME |awk '{print $1}'`
					for i in $FAILN; do echo -e "${WHITE}\t\t$i"; done
				fi
			fi
		else
			if [[ $CLUSTVERS == "4.7" ]]; then
				if [[ $KUB47 == $RHELVERSKUB ]]; then
					if [[ $SILENT == "true" ]]; then
						echo "RHUPGS:0" >> /tmp/scoring_cluster
					else
						echo -e "${WHITE}Check update status RHEL nodes (upgrade step complete):\t\t ${GREEN}pass"
					fi
				else
					if [[ $SILENT == "true" ]]; then
						echo "RHUPGS:1" >> /tmp/scoring_cluster
					else
						echo -e "${WHITE}Check update status RHEL nodes (upgrade step complete):\t\t ${RED}false"
						echo -e "${YELLOW}\tplease check not upgraded nodes:"
						FAILN=`oc --insecure-skip-tls-verify get no -o wide --kubeconfig=/tmp/kbcfg |grep -v $KUB47 |grep -v NAME |awk '{print $1}'`
						for i in $FAILN; do echo -e "${WHITE}\t\t$i"; done
					fi
				fi
			fi
		fi
	fi
fi
}

function CheckVXLANPort {
VXPORT=`oc --insecure-skip-tls-verify get networks.operator.openshift.io cluster --template='{{ .spec.defaultNetwork.openshiftSDNConfig.vxlanPort }}' --kubeconfig=/tmp/kbcfg`
if [[ $VXPORT == "5789" ]]; then
	if [[ $SILENT == "true" ]]; then
		echo "VXLANP:0" >> /tmp/scoring_cluster
	else
		echo -e "${WHITE}Check cluster VXLAN port 5789:\t\t\t\t\t ${GREEN}pass"
	fi
else
	if [[ $SILENT == "true" ]]; then
		echo "VXLANP:1" >> /tmp/scoring_cluster
	else
		echo -e "${WHITE}Check cluster VXLAN port 5789:\t\t\t\t\t ${RED}false"
		echo -e "${YELLOW}\tyou VXLAN port is $VXPORT, please check in networks.operator.openshift.io"
	fi
fi
}

function CheckMTUSize {
MTUSIZE=`oc --insecure-skip-tls-verify get networks.config.openshift.io cluster --template='{{ .status.clusterNetworkMTU }}' --kubeconfig=/tmp/kbcfg`
if [[ $MTUSIZE == "8750" ]]; then
	if [[ $SILENT == "true" ]]; then
		echo "MTUSZE:0" >> /tmp/scoring_cluster
	else
		echo -e "${WHITE}Check cluster MTU is 8750:\t\t\t\t\t ${GREEN}pass"
	fi
else
	if [[ $SILENT == "true" ]]; then
		echo "MTUSZE:1" >> /tmp/scoring_cluster
	else
		echo -e "${WHITE}Check cluster MTU is 8750:\t\t\t\t\t ${RED}false"
		echo -e "${YELLOW}\tyou MTU is $MTUSIZE, please check networks.config.openshift.io"
	fi
fi
}

function CheckDNSRR {
DNSRR=`oc --insecure-skip-tls-verify get cm dns-default -n openshift-dns -o yaml --kubeconfig=/tmp/kbcfg |grep policy |grep sequential |awk '{print $2}'`
if [[ $DNSRR == "round_robin" ]]; then
	if [[ $SILENT == "true" ]]; then
		echo "DNSRRB:0" >> /tmp/scoring_cluster
	else
		echo -e "${WHITE}Check CoreDNS config for use round_robin:\t\t\t ${GREEN}pass"
	fi
else
	if [[ $SILENT == "true" ]]; then
		echo "DNSRRB:1" >> /tmp/scoring_cluster
	else
		echo -e "${WHITE}Check CoreDNS config for use round_robin:\t\t\t ${RED}false"
		echo -e "${YELLOW}\tyour DNS policy is $DNSRR, please use round_robin, see cm dns-default in openshift-dns ns"
	fi
fi
}

if [[ $SILENT != "true" ]]; then
echo -e "\n\n\n\n\n\n............................"
fi

CheckIPDns;
CheckOmegaAuth;
CheckFALSENode;
CheckMasterSchedulable;
CheckVXLANPort;
CheckMTUSize;
CheckMasterHW;
####CheckMasterHDD;
CheckRouterHW;
CheckCertsAPI;
CheckCertsAPPS;
CheckMCPShedState;
CheckCompleteBackup;
CheckCompleteGSync;
CheckRFSRouter;
CheckSMHVers;
#CheckJaegVers;
#CheckKialiVers;
CheckNotLabeledNodes;
CheckRHELUpgrade;
CheckDNSRR;

if [[ $SILENT == "true" ]]; then
	echo > /var/www/clerr_score.html
	SUMMSCORE=0
	for i in `cat /tmp/scoring_cluster |awk -F : '{print $2}'`; do SUMM1=$i && let SUMMSCORE="$SUMMSCORE+$SUMM1"; done
	for i in $SUMMSCORE; do echo $i >> /var/www/clerr_score.html; done
	cp -f /tmp/scoring_cluster /var/www/scoring_detail.html
fi
rm -f /tmp/checkrfs
rm -f /tmp/masterdisk
rm -f /tmp/kbcfg
rm -f /tmp/mcpsilent
rm -f /tmp/routerhealthsilent
rm -f /tmp/masterhealthsilent
alias oc='oc --insecure-skip-tls-verify --kubeconfig=$KUBECONFIG --server=$CLUSTER'

echo -e "${WHITE}\n\nComplete!"